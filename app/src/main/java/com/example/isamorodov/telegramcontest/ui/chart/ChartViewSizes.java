package com.example.isamorodov.telegramcontest.ui.chart;

import android.graphics.Rect;

import static com.example.isamorodov.telegramcontest.utils.AndroidUtilities.dp;

class ChartViewSizes {

    public int pikerHeight = dp(68);
    public int pickerWidth;
    public int chartStart;
    public int chartEnd;
    public int chartWidth;
    public float chartFullWidth;

    public Rect chartArea = new Rect();

    int chartBottomWithPadding;
    int chartBottom;


}
